import React from 'react';
import Data from './data.json';
import './App.css';

function App() {
  
  return (

   <div className="App">
		<div className="posts">
      <h1>Prices</h1>

      {Data.prices.map((price,i)  => {
        return (

    <div key={i} value={price}>
    <p> {price.id}</p>
    <p> {price.price.USD}</p>
    <p> {price.attributes["aws:region"]}</p>
    <p> {price.attributes["aws:label"]}</p>
   </div>
   
        )
      })}
    </div>
    </div>
      );

}

export default App;