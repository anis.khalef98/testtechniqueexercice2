import React, { useEffect, useState } from "react"
import "./App.css";

const App = () => {
  const [services, setServices] = useState([])

  const fetchData = () => {
    fetch(
      "https://dftu77xade0tc.cloudfront.net/fargate-spot-prices.json?timestamp=1625130865034",
    
    )
      .then(response => {
       
        return response.json()
       
      })
      .then(data => {
        setServices(data)
      })
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div>
      {prices.length > 0 && (
        <ul>
          {prices.map(price => (
            <li key={price.id}>{price.price.USD}</li>
          ))}
        </ul>
      )}
    </div>
  )
}

export default App;